require 'susy'
require 'breakpoint'

project_type = :stand_alone
http_path = "/"
sass_dir = "sass"
css_dir = "../builds/development/css"
images_dir = "../builds/development/images"
fonts_dir = "../builds/development/fonts"
javascripts_dir = "../builds/development/js"
output_style = :expanded
relative_assets = true
line_comments = true
